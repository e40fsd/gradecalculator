function findGrade() {
    var markInput = document.getElementById("mark").value;
    var mark = parseFloat(markInput);

    if (isNaN(mark)) {
        document.getElementById("grade").innerHTML = "Please enter a valid numeric mark.";
        return;
    }

    var grade;
    if (mark >= 90) {
        grade = 'A';
    } else if (mark >= 80) {
        grade = 'B';
    } else if (mark >= 70) {
        grade = 'C';
    } else if (mark >= 60) {
        grade = 'D';
    } else {
        grade = 'F';
    }

    document.getElementById("grade").innerHTML = "Grade: " + grade;
}
